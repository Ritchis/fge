<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\models\User;

class LoginController extends Controller
{
    public function create(){
        return view('auth.login');
    }
    
    public function store(){

        $user = User::create(request(['name','email','password']));
        auth()->login($user);
        return redirect()->to('/login');
    }
    public function logout(Request $request)
{
    Auth::logout();

    $request->session()->invalidate();

    $request->session()->regenerateToken();

    return redirect('/');
}

}
