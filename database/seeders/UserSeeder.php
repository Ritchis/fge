<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'=>'Ritchs',
            'email'=>'test@test.com.mx',
            'password'=>bcrypt('123456789'),
        ])->assyngRole($rol1);
    User::factory(15)->create();
    }
}